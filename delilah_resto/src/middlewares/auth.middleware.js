const { verify } = require('jsonwebtoken');
const { jwt } = require('../config/');
const { Users } = require('../database/sync')

const verifyToken = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]
    try {
        const checkToken = verify(token, jwt.key)
        if (checkToken) {
            req.user = checkToken;
            //console.log(`Verified token.`)
            return next()
        }
    } catch (error) {
        if (error.name === 'TokenExpiredError') {
            res.status(401).send({ error: 'Unauthorized.', message: 'Token expired.' })
        } else {
            res.status(401).send({ error: 'Unauthorized.', message: 'Token verification failed.' })
        }
    }
}
/* Verifica campos vacios en el Login y en el registro*/
function verifyNull(req, res, next) {
    let { username, fullName, email, phone, address, password } = req.body
    switch (req.url) {

        case '/sign-up':
            if (!username || !fullName || !email || !phone || !address || !password) {
                res.status(401).send('Todos los campos son requeridos')
            } else {
                next();
            }
            break;
        case '/login':
            if (!username || !password) {
                res.status(401).send('Todos los campos son requeridos')
            } else {
                next();
            }
            break;
    }
};
// Verificar que el username & mail no esten duplicado
async function verifiyMailDuplicate(req, res, next) {
    let { email, username } = req.body;
    const mailDuplicate = await Users.findOne({ where: { email: email } });
    if (mailDuplicate) {
        res.status(402).send('El mail ya se encuentra registrado')
    } else {
        const userDuplicate = await Users.findOne({ where: { username: username } });
        if (userDuplicate) {
            res.status(402).send('El usuario ya se encuentra registrado, intente con un nombre diferente.');
        } else {
            next();
        }
    };
}

//Middleware que verifiica si el usuario es un administrador
function verifyRole(req, res, next) {
    const token = req.headers.authorization.split(' ')[1]
    // [false] => User (Client)
    // [true] => Adminstrador 
    try {
        const data = verify(token, jwt.key);
        if (data.is_admin) {
            next();
        } else {
            res.status(403).send({ error: 'Unauthorized.', message: 'Access denied.' })
        }

    } catch {
        res.status(401).send({ error: 'Unauthorized.', message: 'Token verification failed' })

    }
};

module.exports = { verifyNull, verifiyMailDuplicate, verifyToken, verifyRole }