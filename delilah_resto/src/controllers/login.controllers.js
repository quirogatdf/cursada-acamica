const { Users } = require("../database/sync");
const { jwt } = require('../config');
const { sign } = require('jsonwebtoken')

class LoginController {
    /* Control de inicio de sesión */
    static async login(req, res) {
        const { username, password } = req.body
        const findUser = await Users.findOne({
            where: {
                username: username,
                //password: password
            }, raw: true
        })
        console.log(findUser);

        if (findUser) {
            if (findUser.password === password) {
                if (findUser.is_active) {
                    /* Generar Token */
                    const newToken = sign(findUser, jwt.key, {
                        expiresIn: jwt.expires_time
                    })
                    /* Guardar Token en la BD */
                    await Users.update({
                        token: newToken
                    }, {
                        where: { id: findUser.id }
                    })
                    res.status(201).send({ messsage: `Bienvenido, ${findUser.fullName}`, token: newToken });
                } else {
                    res.status(404).send(`El usuario ${findUser.username} se encuentra inhabilitado, consulte al administrador`)
                }
            } else {
                res.status(404).send('Contrasenia inválida')
            }

        } else {
            res.status(404).send('Usuario Inválido')

        }
    }

}

module.exports = LoginController