const { OrderDetails, Orders, Products } = require("../database/sync");

class OrderDetailsControllers {
    /* Agregar y/o modificar productos del pedido pendiente */
    static async add(req, res) {
        const { productsId, quantity } = req.body
        //Verifica que el usuario tenga un pedido pendiente
        const pendiente = await Orders.findOne({
            where: {
                userId: req.user.id,
                state: 'Pendiente'
            }
        })
        if (pendiente) {
            //Verifica si el producto se cargo previamente
            const exists = await OrderDetails.findOne({
                where: {
                    ordersId: pendiente.id,
                    productsId: productsId
                }
            })
            if (exists) {
                // Se actualiza la cantidad y el subtotal del producto.
                let product = await Products.findByPk(productsId);
                let Qty = parseInt(quantity) + parseInt(exists.quantity);
                let subtotal = Qty * product.precio;
                console.log(subtotal)
                await OrderDetails.update({
                    subtotal: subtotal,
                    quantity: Qty
                }, {
                    where: {
                        id: exists.id
                    }
                });

                res.status(200).send({message:`Se actualizo la cantidad del producto ${product.name} `})
            } else {
                let product = await Products.findByPk(productsId)
                if (product){
                    res.status(404).send({message:'Not found Id.'})
                }
                let subtotal = quantity * product.precio
                await OrderDetails.create({
                    quantity: quantity,
                    subtotal: subtotal,
                    productsId: productsId,
                    ordersId: pendiente.id
                })
                res.status(200).send({message:`Se agregó ${quantity} - ${product.name} a su pedido`})
            }
        } else {
            res.status(404).send(`No se encontro un pedido pendiente`)
        }
    }
    /*Muestra el detalle del pedido del usuario*/
    static async getAll(req, res) {
        const pendiente = await Orders.findOne({
            where: {
                userId: req.user.id,
                id: req.params.orderid,
                state: 'Pendiente'
            }
        })
        console.log(req.params.orderid);
        if (pendiente) {
            let orderDetail = await OrderDetails.findAll({
                where: {
                    ordersId: pendiente.id,
                }
            });
            res.status(201).send({title:`Detalle del pedido #${pendiente.id}`, OrderDetail:orderDetail})
        } else {
            res.status(404).send(`No se encontro un pedido pendiente`)
        }

    }
    /*Borrar un producto del pedido*/
    static async delete(req, res) {
        const pendiente = await Orders.findOne({
            where: {
                id: req.params.orderid,
                userId: req.user.id,
                state: 'Pendiente'
            }
        })

        if (pendiente) {
            await OrderDetails.destroy({
                where: {
                    id: req.params.id
                }
            })
                .then((deletedRecord) => {
                    if (deletedRecord === 1) {
                        res.status(200).json({ message: 'Se borro el producto del pedido' })
                    } else {
                        res.status(404).json({ error: 'Not found Id.' })
                    }
                })
                .catch((error) => {
                    res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
                })

        } else {
            res.status(404).send('No se encontró un pedido pendiente.')
        }

    }
}

module.exports = OrderDetailsControllers