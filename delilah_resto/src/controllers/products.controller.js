const { Products } = require('../database/sync');
const { REDIS } = require('../config/index')
const redis = require('redis');
//Configurando servidor Redis.
const client = redis.createClient({
  host: REDIS.HOST,
  port: REDIS.PORT

})
client.on('error', (err) => {
  console.log(err)
})
// Verifica que redis inicie correctamente
client.on('connect', () => {
  console.log(`Conectado a redis en el puerto ${REDIS.PORT}`)
})

class ProductsController {
  /* Listar todos los productos */
  static async getAll(req, res) {
    //Verifico si los productos existen en caché
    client.get('productos', async (err, data) => {
      if (err) {
        //Muestra si hubo un error
        res.status(404).send(err)
      };
      if (data) {
        //Existe la clave, devuelve los productos guardados en caché
        res.status(200).send({
          title: 'Listado de productos en caché',
          products_data: JSON.parse(data)
        })
      } else {
        console.log('No existe la llave, se creara una nueva');
        //Si no están, los busco en la DB.
        const productosDB = await Products.findAll();
        //Guardo los productos dentro del caché x 1h, para una futura consulta
        client.setex('productos', 60 * 60, JSON.stringify(productosDB), (error) => {
          if (error) {
            return res.status(404).send(error);
          };
          //Devuelvo los productos desde la BD.
          res.status(200).send({
            title: 'Listado de productos',
            products_data: productosDB
          });
        });
      }
    });
  }

  /* Listar producto seleccionado */
  static async getById(req, res) {
    const id = parseInt(req.params.id);
    let product = await Products.findByPk(id)
    if (product) {
      res.status(201).send(product)
    } else {
      res.status(500).json({ message: 'No se encontró el producto seleccionado' })
    }
  }

  /* Agregar un producto nuevo */
  static async add(req, res) {
    const { nombre, description, precio } = req.body;
    try {
      await Products.create({
        name: nombre,
        precio: precio,
        description: description,
      })
      res.status(201).json({ message: `se ha creado con éxito el producto: ${nombre}` })
    } catch (error) {
      res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
    }
  }

  /* Actualizar el registro del producto seleccionado */
  static async updateRegistry(req, res) {
    const id = parseInt(req.params.id);
    try {
      const producto = await Products.findByPk(id)
      console.log(producto)
      if (producto) {
        await Products.update(req.body, {
          where: { id: id }
        });
        //Si se modifica el precio de un producto, se limpia el cáche.
        if (req.body.precio) {
          client.exists('productos', (err, data) => {
            if (data === 1) {
              client.del('productos', (err, deleted) => {
                if (deleted === 1) {
                  console.log(`Se limpio el cáche correctamente.`)
                };
              })
            };
          })
        };

        res.status(201).json({ message: `Producto actualizado` })
      } else {
        res.status(404).json({ error: 'Not found Id.' })
      }
    } catch (error) {
      res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
    }
  }

  /* Eliminar el producto seleccionado */
  static async delete(req, res) {
    const id = parseInt(req.params.id);
    await Products.destroy({
      where: {
        id: id
      }
    })
      .then((deletedRecord) => {
        if (deletedRecord === 1) {
          res.status(201).json({ message: 'Producto borrado satisfactoriamente.' })
        } else {
          res.status(404).json({ error: 'Not found Id.' })
        }
      })
      .catch((error) => {
        res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
      })
  }
}

module.exports = ProductsController