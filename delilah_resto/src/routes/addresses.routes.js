const { Router } = require ('express')
const AddressesControllers= require('../controllers/addresses.controllers')
const {verifyToken, verifyRole} = require('../middlewares/auth.middleware')

const router = Router()

router
    .post('/add',verifyToken, AddressesControllers.add)                                  /*Create*/
    .get('/', verifyToken, AddressesControllers.getAll)
    .get('/:id', verifyToken, AddressesControllers.getById)                              /*Read*/
    .put('/edit/:id', verifyToken, AddressesControllers.updateRegistry)      /*Update*/
    .delete('/delete/:id', verifyToken, AddressesControllers.delete)         /*Delete*/

module.exports = router