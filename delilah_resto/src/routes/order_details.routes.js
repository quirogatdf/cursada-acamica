const router = require('express').Router({mergeParams:true}) //mergeParams: conserva los valores del req.params del enrutador principal
const OrderDetailsControllers = require('../controllers/order_details.controllers')
const { verifyToken } = require('../middlewares/auth.middleware')

//const router = Router()

router
    .post('/add', verifyToken, OrderDetailsControllers.add)                                  /* Cargar un nuevo producto y/o modificar la cantidad, antes de confirmarlo*/
    .get('/', verifyToken, OrderDetailsControllers.getAll)                      /*El usuario puede ver el detalle de su pedido pendiente*/
    .delete('/delete/:id', verifyToken, OrderDetailsControllers.delete)         /*El usuario puede eliminar un producto del pedido, antes de confirmarlo*/

module.exports = router