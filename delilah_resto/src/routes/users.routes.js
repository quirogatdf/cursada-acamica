const { Router } = require ('express');
const UsersController = require('../controllers/users.controller')
const { verifyNull, verifiyMailDuplicate, verifyToken, verifyRole} = require('../middlewares/auth.middleware')

const router = Router()

router
    .post('/sign-up', verifyNull, verifiyMailDuplicate, UsersController.add)                  /*Los clientes pueden generar un nuevo usuario*/
    .get('/', verifyToken, verifyRole, UsersController.getAll)                                /*El administrador puede ver todos los usuarios*/
    .get('/:id', verifyToken, UsersController.getById)                                        /*Los usuarios puede visualizar sus datos*/ 
    .put('/suspend/:id', verifyToken, verifyRole, UsersController.suspend)                    /*El administrador puede suspender a un usuario*/
    .put('/edit/:id', verifyToken, verifyRole, UsersController.updateRegistry)                /*El administrador puede modificar los datos del usuario*/
    .delete('/delete/:id',verifyToken, verifyRole, UsersController.delete)                    /*El administrador puede eliminar un usuario*/

module.exports = router