require ('dotenv').config();

module.exports = {
    DB: {
        USERNAME: process.env.MYSQL_USER,
        PASSWORD: process.env.MYSQL_PASSWORD,
        DATABASE: process.env.MYSQL_DATABASE,
        HOST: process.env.MYSQL_HOST,
        DIALECT: process.env.MYSQL_DIALECT
    },
    SERVER: {
        PORT: process.env.SERVER_PORT || 3000
    },
    jwt : {
        key: process.env.PRIVATE_KEY,
        expires_time: process.env.EXPIRES_TIME
    },
    REDIS : {
        HOST : process.env.REDIS_HOST,
        PORT : process.env.REDIS_PORT
    }
}