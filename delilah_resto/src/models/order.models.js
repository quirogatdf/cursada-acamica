module.exports = (sequelize, type) => {
    return sequelize.define('Orders', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        date_time: {
            type: type.DATE,
            defaultValue: type.NOW
        },
        description: type.STRING,
        total: {
            type: type.FLOAT,
            defaultValue: null
        },
        state: {
            type: type.STRING(45),
            defaultValue: 'Pendiente'
        },
    },
        {
            sequelize,
            timestamps: false,
        }
    )
}