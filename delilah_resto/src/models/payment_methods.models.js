module.exports = (sequelize, type) => {
    return sequelize.define('Payment_method', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: type.STRING(50),
        },
        {
            sequelize,
            timestamps: false,
        }
    )
}