module.exports = (sequelize, type) => {
    return sequelize.define('Products', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: type.STRING(50),
        description: type.STRING(50),
        precio: type.FLOAT,
        },
        {
            sequelize,
            timestamps: false,
        }
    )
}