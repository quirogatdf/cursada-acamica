const express = require('express');
const helmet = require('helmet')                   
const { SERVER } = require('./src/config');       //Llamado a la variable de entorno
const swaggerJsDoc = require('swagger-jsdoc');    //Swagger
const swaggerUI = require('swagger-ui-express');  //Swagger

/* Configuramos nuestro servidor express */
const app = express();

/* Realizamos llamado a nuestra Base de Datos */
require('./src/database/db');
//require('./src/database/sync');


/* Permite recibir parámetros en formato JSON */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/*Agregar seguridad a nuestro server usando Helmet*/
app.use(helmet());

/* Configuramos  nuestras rutas */
const apiRouter = require('./src/routes/api');
app.use('/', apiRouter);

/* Configuraciones de Swagger */
const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Mi Primera API',
            description: 'API para gestión de pedidos de un restaurante. Los usuarios pueden registrarse, loguearse, ver el listado de productos y realizar/consultar pedidos. Los administradores pueden realizar operaciones CRUD sobre usuarios, productos y ordenes',
            version: '1.0.0'
        },
        servers: {
            url: 'https://localhost:3000'
        }
    },
    apis: ['./src/docs/swagger.js'],
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs)
);

/*Iniciando servidor */
app.listen(SERVER.PORT, () => {
    console.log(`Servidor ejecuntadose en el puerto ${SERVER.PORT}`)
})

module.exports = app