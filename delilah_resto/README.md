# Mi primera API: Delilah Resto
Sprint 02: "Mi APP persistente", del curso de DWBE de Acamica

## Tecnologías utilizadas
* Node JS
* Express
* JWT
* Helmet
* MYSQL
* Sequelize
* Nodemon (opcional)
* Redis para almacenar la información de los productos en caché. 
* Mocha para realizar pruebas unitarias en el registro de usuarios
* Postman para testear los endpoints
* Swagger para crear la documentacion

**Consejo:** _Instalar **nodemon** como entorno de desarrollo nos permite que el reinicio del servidor sea automático_
  
  `npm install -g nodemon`

## Instrucciones de instalación
### 1. Clonar repositorio
Abrir la terminal en la carpeta donde se va a clonar el proyecto y escribir el siguiente comando: 

`git clone https://gitlab.com/quirogatdf/cursada-acamica.git `

y presione Enter.

### 2. Instalar dependencias
Las dependencias se instalan con el siguiente comando:
`npm install`

**Express**

`npm install express --save`

**Swagger**

`npm install swagger-ui-express --save`

`npm install swagger-jsdoc --save`

**Mocha**

`npm install mocha --save`

`npm install chai --save`

**dotenv**

`npm install dotenv --save`

**Helmet**

`npm install helmet --save`

**JSON Web Token**

`npm install jsonwebtoken --save`

**node-fetch**

`npm install node-fetch@2.6.2 --save`

**MySQL**

`npm install mysql2 --save`
`npm install sequelize --save`


**Redis (Caché)**

`npm install redis --save`

### 3. XAMPP
Instalar Xampp ó algun otro sistema de gestión de bases de datos MySQL.
Una vez instalado XAMPP se debera tener activado los módulos Apache y MySQL, para crear la base de datos y posteriormente para realizar consultas a la base de datos. 
#### a. Crear base de datos
Es necesario crear nuestra DB, para poder realizar las consultas. Para ello accedemos a [http://localhost/phpmyadmin/](http://localhost/phpmyadmin/), hacemos clic en la opción "Nueva" y en la opción desplegada colocamos el nombre de nuestra BD "delilahdb" y la codificación que usaremos.

#### b. Cargar base de datos

Creada la bases de datos, importaremos el archivo "db.sql" ubicado en la carpeta "db". La base de datos cuenta con datos de usuarios (1 administrador y 1 usuario habilitado y 1 usuario suspendido), direcciones precargadas para cada usuario y productos listos para realizar las consultas que precise. 

### 4. Configurar parametros
Deberá inicializar las siguientes variables de entorno para el correcto funcionamiento del sistema. Para ello, crearemos en el raíz un archivo .env con las siguientes variables
> #database
> 
> MYSQL_HOST     = localhost
>
> MYSQL_USER     = root
>
> MYSQL_DATABASE = delilahdb
>
> MYSQL_PASSWORD = 
>
> MYSQL_DIALECT  = mysql
> 
>
> #Express
>
> SERVER_PORT    = 3000
> 
>
> #JWT
>
> PRIVATE_KEY    = MiLLavePrivada
>
> EXPIRES_TIME   = '2h'
> 
>
> #REDIS
>
> REDIS_HOST     = localhost
>
> REDIS_PORT     = 6379

Es importante, que verifique que los valores coincidan con los parametros que haya definido al momento de la instalación de cada servicio, de otra manera puede que al momento de ejecutar el servidor presente fallas.

### 5. Iniciar servidor
Para iniciar el servidor, es necesario que desde la consola nos ubiquemos en el raíz de nuestro proyecto y ejecutemos:

`npm start`

En caso de que hayan instalado **nodemon**, los siguientes comando también son válidos:

`nodemon dev` ó `npm run dev`

Si la consola devuelve el siguiente mensaje _"Servidor iniciado en el puerto 3000."_ ya esta listo para realizar las consultas desde Postman.

### 4. Consultas
**Postman**

Puede realizar las consultas de manera independiente, o puede importar la siguiente colección con todos los endpoint creados.

Coleccion Postman: [https://www.getpostman.com/collections/6237ad29b4fe499841d2](https://www.getpostman.com/collections/6237ad29b4fe499841d2)

**Swagger**

Para acceder a la documentación, debe ingresar desde un navegador web al siguiente link [http://localhost:3000/api-docs](http://localhost:3000/api-docs)

**Mocha**

Para realizar las pruebas de test con Mocha, es necesario que el servidor se encuentre corriendo en el puerto 3000. Luego desde la consola ejecutaremos el comando:

`npm test`
